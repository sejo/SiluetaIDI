class HandsTracker{
 ArrayList<KSkeleton> skeletonArray;
 ArrayList<PVector> points;
 
 int kw, kh; // Kinect width and height
 int w, h;
 
 HandsTracker(int screenwidth, int screenheight){
       points = new ArrayList<PVector>(); 
       kw = kinect.WIDTHDepth;
       kh = kinect.HEIGHTDepth;
       
       w = screenwidth;
       h = screenheight;
 }
 
 ArrayList<PVector> getPoints(){
    return points;
  }
 
 void update(ArrayList<KSkeleton> sk){
  skeletonArray = sk;
  points.clear();
  for (int i = 0; i < skeletonArray.size(); i++) {
    KSkeleton skeleton = (KSkeleton) skeletonArray.get(i);
    //if the skeleton is being tracked compute the skleton joints
    if (skeleton.isTracked()) {
      KJoint[] joints = skeleton.getJoints();

      color col  = skeleton.getIndexColor();
      fill(col);
      stroke(col);
      
      processHand(joints,KinectPV2.JointType_HandRight, KinectPV2.JointType_HandTipRight);
      processHand(joints,KinectPV2.JointType_HandLeft, KinectPV2.JointType_HandTipLeft);
      
    }
  }
   
 }
 
 void processHand(KJoint[] joints, int handJointIndex, int handTipJointIndex){
      KJoint hand = joints[handJointIndex];
      KJoint handTip = joints[handTipJointIndex];
      if(hand.getState()==KinectPV2.HandState_Lasso){
        float cx = map(handTip.getX(),0,kw,0,w);
        float cy = map(handTip.getY(),0,kh,0,h);
        points.add(new PVector(cx,cy));
      }
 }
  
  
}