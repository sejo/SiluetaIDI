class Particle{
 PVector p;
 PVector v;
 PVector a;
 
 float angle;
 
 PVector attpos;
 
 float life;
 float maxlife; 
 float maxforce;
 float maxspeed;
 
 float size;
 
 color col;
 
 boolean reached;
 boolean hasAttractor;
 Particle(){
   
 }
 
 Particle(PVector attractorpos, boolean withAttractor){
   hasAttractor = withAttractor;
   if(hasAttractor){
  float maxrad = 20;
  attpos = new PVector();
  attpos.set(attractorpos);
  
  float r = randomGaussian()*maxrad;//random(0,maxrad);
  float f = random(0,TWO_PI);
  
  p = new PVector(r*cos(f)+attpos.x,r*sin(f)+attpos.y);
  col = color(255);
  size = random(10,50);
   }
   else{
    p = new PVector();
    p.set(attractorpos);
       int c = int(random(0,3));
   // Colores: http://www.colourlovers.com/palette/218483/Christmas_Lights
   switch(c){
      case 0:
        col = color(#D61818);
        break;
      case 1:
       col = color(#348818);
      break;
      case 2:
      col = color(#D4BF1E);
      break;
   }
   size = random(10,50);
   }
  v = PVector.random2D();
  v.mult(2);
  a = new PVector(0,0);
 updateAngle();
  maxlife = 50;
  life = maxlife;
  
  maxforce = 0.1;
  maxspeed = 5;
  
  reached = false;
   

   
 }
 
 void updateAngle(){
  angle = map(p.x,0,width,-TWO_PI, TWO_PI); 
 }
 
 void repel(PVector repeller){
  PVector desired = PVector.sub(p,repeller);
  float m = desired.mag();
  float radio = 250;
  if(m<radio){
  desired.normalize();
  desired.mult(map(radio-m,0,radio,0,maxspeed));
  PVector force = PVector.sub(desired,v);
  force.limit(maxforce);
  a.add(force);
  }
   
 }
 
 void attract(){
  PVector desired = PVector.sub(attpos,p);
  float m = desired.mag();
  if(m<10){
   reached = true; 
  }
  desired.normalize();
  desired.mult(map(m,0,100,0,maxspeed));
  
  PVector force = PVector.sub(desired,v);
  force.limit(maxforce);
  a.add(force);
 }
 
 void wander(){
  PVector f = PVector.random2D();
  f.mult(0.8);
  a.add(f);
 }
 
 void update(){
   if(hasAttractor && !reached){
     attract();
   }else{
    wander(); 
   }
   v.add(a);
   p.add(v);
   a.mult(0);
   life--;
   updateAngle();
 }
 
 boolean isDead(){
  return life<0; 
 }
 
 boolean isOutOfScreen(){
  return p.x>width || p.x<0 || p.y > height || p.y < 0; 
 }
 
 void draw(){
   noStroke();
   fill(map(life,0,maxlife,0,255));
  ellipse(p.x,p.y,5,5); 
 }

 void draw(PGraphics pg){
   pg.noStroke();
  /* if(reached){
   pg.fill(col,map(life,0,maxlife,0,255));
   }else{*/
  pg.fill(col,map(life,0,maxlife,0,255));
 //  }
  pg.ellipse(p.x,p.y,size,size); 
 }
 
 void draw(PGraphics pg, PImage img){
  pg.imageMode(CENTER);
  pg.tint(col,map(life,0,maxlife,0,255));
  pg.pushMatrix();
  pg.translate(p.x,p.y);
  pg.rotate(angle);
  pg.image(img, 0,0, size, img.height*size/img.width);
  pg.popMatrix();

   
 }
 
 void setLife(int l){
  maxlife = l;
  life = maxlife;
   
 }
  
  
}