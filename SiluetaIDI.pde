import processing.video.*;
import gab.opencv.*;
import KinectPV2.*;


Movie movie;


Silueta s;

Capture cam;

KinectPV2 kinect;
HandsTracker handsTracker;

PShader texShader;
PShader texShader2;
// Quitar número
PGraphics fb; // Glow map
PGraphics pg;
PGraphics pg2;

ParticleSystem ps;


boolean showText;
boolean doSave;

void setup() {
  size(1280,720,P3D);
  //fullScreen(P3D);
  ps = new ParticleSystem("Huella_Transparent.png");

  
  

  /*
  cam = new Capture(this,1280,720,"/dev/video0",30);
  cam.start();*/
    kinect = new KinectPV2(this);
  kinect.enableColorImg(true);
  kinect.enableDepthImg(true);
  kinect.enableBodyTrackImg(true);
  
    //Enables depth and Body tracking (mask image)
  kinect.enableDepthMaskImg(true);
  kinect.enableSkeletonDepthMap(true);

  kinect.init();
  
  println("Color: "+kinect.WIDTHColor+" "+kinect.HEIGHTColor);
  println("Depth: "+kinect.WIDTHDepth+" "+kinect.HEIGHTDepth);
  
 
  

  
  s = new Silueta(this,width,height,kinect.WIDTHDepth,kinect.HEIGHTDepth);
  
  handsTracker = new HandsTracker(width,height);
  
    texShader = loadShader("verticalblur_texfrag.glsl");
  texShader2 = loadShader("horizontalblur_texfrag.glsl");  
  texShader.set("N",5); // Vertical
  texShader2.set("N",5); //Horizontal
  fb = createGraphics(width,height,P3D);
  pg = createGraphics(640,360,P3D);
  pg2 = createGraphics(640,360,P3D);
  
  frameRate(60);
  
  showText = true;
  doSave = false;
  
  movie = new Movie(this,"stars.mov");
//  movie.start();
  movie.loop();

}

void draw() {


  s.update(kinect.getBodyTrackImage());
  
  handsTracker.update(kinect.getSkeletonDepthMap());



      ps.update();
      

  
  
  //background(0);
blendMode(BLEND);

// Draw background
    if (movie.available() == true) {
    movie.read(); 
  }
  image(movie, 0, 0, width, height);

    
    
    // Glow
    fb.beginDraw();
    fb.clear();
 //   fb.image(c.getAcc(),0,0,width,height);

    s.draw(fb);
    ps.draw(fb);
    fb.endDraw();
    

    
      // Horizontal y vertical blur sobre fb
  pg.beginDraw();
  pg.shader(texShader2);
  pg.image(fb,0,0,pg.width,pg.height);
  pg.endDraw();
  pg2.beginDraw();
  pg2.shader(texShader);
  pg2.image(pg,0,0,pg2.width, pg2.height);
  pg2.endDraw();
    
    blendMode(SCREEN);
    //image(c.getFilteredImage(),0,0,width,height);

 
    image(pg2,0,0,width,height);
     image(fb,0,0);
    


    

    
    blendMode(BLEND);
  //    ArrayList<PVector> points = c.getPoints();
      ArrayList<PVector> points = handsTracker.getPoints();      
stroke(255,255,0);
fill(255,0,0);
  for(int i=0; i<points.size();i++){
    PVector p = points.get(i);
     ps.addMany(p);

  //  text(i,p.x+15, p.y);
 //  ellipse(p.x,p.y,10,10); 
  }
  
  
    if(showText){
     fill(255,0,0);
  textSize(20);
    text(frameRate,50,50);

    text(doSave?"saving":"not saving",50,150);
  }


//  if(doSave && frameCount%300==0 && ps.isFull()){
  if(doSave){
   saveFrame(); 
  }


}

void mousePressed(){

}

void keyPressed(){
  switch(key){

   case 'Q':
   /*
     if(showText == true){
       showText = false;
     } else{
       showText = true;
     }*/

     showText = !showText;
   break;
   case 's':
     doSave = !doSave;
     println("Do Save: " + (doSave?"enabled":"disabled"));
     break;
    
  }

}