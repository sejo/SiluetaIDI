// Toma valor de una textura de 1D y toma el pixel desplazado
#define PROCESSING_TEXTURE_SHADER
#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

uniform sampler2D texture;
uniform vec2 texOffset;

uniform sampler2D values;
uniform float texwidth;
uniform float texheight;
uniform float magYfactor;

varying vec4 vertColor;
varying vec4 vertTexCoord;
varying vec4 vert;


void main() {
vec4 col = texture2D(texture, vertTexCoord.st);

float magY = texheight*magYfactor;


vec4 value = texture2D(values,vec2(vertTexCoord.s,1));
vec2 next = vertTexCoord.st + vec2(0,(magY*value.r-magY/2)*texOffset.t); // lineal

// Si se pasa de los bordes, da la vuelta
int aux;

if(next.t>1.0){
	aux = int(next.t);
	next = next + vec2(0,-float(aux));
}
else if(next.t<0){
	aux = int(-next.t);
	next = next + vec2(0,float(aux+1));
}

vec4 colnext = texture2D(texture, next);


gl_FragColor = vec4(colnext.rgb,1.0)*vertColor;

  }
