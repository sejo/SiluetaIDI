class ParticleSystem{
 ArrayList<Particle> ps;
 int maxsize;
 
 PImage img;
 
 ParticleSystem(){
  this("");
 }
 
 ParticleSystem(String imagePath){
     ps = new ArrayList<Particle>(); 
  maxsize= 3000;
  if(!imagePath.equals("")){
  img = loadImage(imagePath);
  }
 }
 
 void update(){
  for(int i=0;i<ps.size();i++){
    Particle p = ps.get(i);
   p.update();
   if(p.isDead()){
    ps.remove(i); 
   }
  } 
 }
 
 void updateRenew(){
  for(int i=0;i<ps.size();i++){
    Particle p = ps.get(i);
   p.update();
   if(p.isDead() || p.isOutOfScreen()){
    ps.remove(i); 
    addRandom();
   }
  } 
   
 }
 
  void draw(PGraphics pg){
  for(int i=0;i<ps.size();i++){
    Particle p = ps.get(i);
   //  p.draw(pg); 
     p.draw(pg,img);
  }
  pg.imageMode(CORNER);
  pg.tint(255);
 }
 
 void addMany(PVector attpos){
   int n = int(random(1,5));
   for(int i=0;i<n;i++){
    ps.add(new Particle(attpos,true)); 
   }
   
   while(ps.size()>maxsize){
    ps.remove(0); 
   }
 }
  
  
 void addRandom(){ // Without attactor
   PVector pos = new PVector(random(width),random(height));
   ps.add(new Particle(pos,false));
   ps.get(ps.size()-1).setLife(int(random(100,500)));
   ps.get(ps.size()-1).maxspeed = 20;
      ps.get(ps.size()-1).maxforce = 4;
 
 }
 
 void repel(PVector repeller){
     for(int i=0;i<ps.size();i++){
    Particle p = ps.get(i);
     p.repel(repeller); 
  }
   
 }
  
 int size(){
  return ps.size(); 
 }
 
 boolean isFull(){
  return size()>=maxsize-1; 
 }
}