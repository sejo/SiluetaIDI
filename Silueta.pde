import processing.video.*;
import gab.opencv.*;
import java.awt.Rectangle;

class Silueta{
  OpenCV opencv;
  int screenwidth,screenheight;
  ArrayList<Contour> contours;
  
  ArrayList<PVector> points;
  ArrayList<PVector> ppoints; // Previous points

   PGraphics buf;
  
  int cw, ch; // cam width and height
  
  Silueta(PApplet pa, int w, int h, int camw, int camh){
    screenwidth = w;
    screenheight = h;
    cw = camw;
    ch = camh;
    
     opencv = new OpenCV(pa,camw,camh);


     points = new ArrayList<PVector>();
     
     buf = createGraphics(screenwidth,screenheight);

  }
  
  void update(PImage img){
    PImage im = new PImage(cw,ch);
    im.copy(img,0,0,img.width,img.height,0,0,cw,ch);

   opencv.loadImage(im);
   opencv.invert();
   opencv.threshold(100);

 

  contours = opencv.findContours(true,true);
  

    buf.beginDraw();
    buf.clear();
    buf.fill(255,20);
    buf.strokeWeight(5);
    buf.stroke(255,100);//128
    buf.noStroke();
    for (Contour contour : contours) {
    contour.setPolygonApproximationFactor(2);
    
    buf.beginShape();
    for (PVector point : contour.getPolygonApproximation().getPoints()) {
       float cx = map(point.x,0,im.width,0,screenwidth);
      float cy = map(point.y,0,im.height,0,screenheight);
           //  println(point.y + " "+cy);
   //   ellipse(cx,cy,10,10);
      buf.vertex(cx,cy);
    }
    buf.endShape(CLOSE);

    

    } // for
    buf.endDraw();
   // image(buf,0,0);
  } //update

  void draw(PGraphics pg){
     pg.image(buf,0,0);
    
  }

  
}