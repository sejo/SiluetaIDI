// Blur horizontal

#define PROCESSING_TEXTURE_SHADER
#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

uniform sampler2D texture;
uniform vec2 texOffset;

uniform int N; // Size of the blur

varying vec4 vertColor;
varying vec4 vertTexCoord;
varying vec4 vert;


void main() {

float ptr;
int count = 0;
vec4 color = vec4(0,0,0,0);
for(int i=0; i<N*2+1; i++){
	ptr = (i-N)*texOffset.s + vertTexCoord.s;
	if(ptr>=0 && ptr<=1){ // If within range
		color += texture2D(texture,vec2(ptr,vertTexCoord.t));
		count++;
	}

}
color = color/(1.0*count);
color = vec4(color.rgb, 1.0);
gl_FragColor = color*vertColor;
  }
